package config

import (
 "fmt"
 "os"
 "encoding/json"
)


// Config
// builds and returns struct named Config that contains server and database 
// information this information is built from the DefaultConfig() function 
// or from a file named .config located in the root director of the application.
// The preference is to use the .config file
//
// Usage:
// 
// import "config"
// cfg := LoadConfig()
// fmt.Println(cfg)

// Config struct exports the server setup information

type Config struct {
 Port  int  `json:"port"`
 DbHost	string  `json:"dbhost"`
 DbPort	int  `json:"dbport"`
 DbName	string  `json:"dbname"`
}

// DefaultConfig returns the default server and database 
// information if a .config file is not available

func DefaultConfig() Config {
 return Config{
  Port:  8080,
  DbHost:  "localhost",
  DbPort:  0,
  DbName:  "data.db",
  }
}

// LoadConfig checks if a config file exists, if so return the Config struct populated
// with this data. If not a call to DefaultConfig() returns the default information.

func LoadConfig() Config {
 f, err := os.Open(".config")
 if err != nil {
  fmt.Println("Config file not found Using default config")
  return DefaultConfig()
 }
 var c Config
 dec := json.NewDecoder(f)
 err = dec.Decode(&c)
 if err != nil {
  panic(err)
 }
 fmt.Println("Config file found and loaded successfully")
 return c
}



